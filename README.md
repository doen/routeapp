# iOS routeApp #

A simple app to create a locations itinerary and present the route on a map.

### Functionality? ###

* Map showing current location
* Reorderable Itinerary
* Search for places
* Adding places to Itinerary
* Sharing and bookmarking itineraries
* Fetching/showing route on map with duration information for different transport modes
* Longpress map gesture for dropping pin at touch location (and adding that location to itinerary)
* Universal support (iPhone/iPad)

### How do I get set up? ###

* Clone project
* Open routeApp.xcodeproj
* Build and run

### Dependencies ###

* iOS 8.0 or higher