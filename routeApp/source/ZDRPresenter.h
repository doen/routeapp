//
//  ZDRPresenter.h
//  routeApp
//
//  Created by Ziga Dolar on 25/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIViewController;

@interface ZDRPresenter : NSObject

+ (UIViewController*)rootViewController;
+ (UIViewController*)topmostViewController;

@end
