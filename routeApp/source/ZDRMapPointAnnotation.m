//
//  ZDRMapPointAnnotation.m
//  routeApp
//
//  Created by Ziga Dolar on 26/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRMapPointAnnotation.h"

#import "Waypoint.h"

@implementation ZDRMapPointAnnotation

- (void)setWaypoint:(Waypoint *)waypoint
{
    _waypoint = waypoint;
    
    [self configurePointAnnotationForWaypoint:waypoint];
}

- (void)configurePointAnnotationForWaypoint:(Waypoint*)waypoint
{
    self.coordinate = waypoint.location.coordinate;
    self.title = waypoint.title;
    self.subtitle = waypoint.address;
}

@end
