//
//  ZDRItineraryManager.h
//  routeApp
//
//  Created by Ziga Dolar on 26/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Itinerary, Waypoint;

@protocol ZDRItineraryManagerDelegate <NSObject>

- (void)currentItineraryChanged;

@end


@interface ZDRItineraryManager : NSObject

+ (instancetype)sharedManager;

@property (nonatomic, weak) id<ZDRItineraryManagerDelegate> delegate;

@property (nonatomic, strong) Itinerary *currentItinerary;

- (void)addWaypointToItinerary:(Waypoint*)waypoint;
- (void)removeWaypointAtIndex:(NSUInteger)atIndex;
- (void)moveWaypointFromIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex;

- (void)removeWaypointFromItinerary:(Waypoint*)waypoint;

- (void)setTransportModeCar;
- (void)setTransportModePedestrian;
- (void)setTransportModePublicTransport;

+ (void)addCurrentItineraryToBookmarksWithName:(NSString*)bookmarkName;
+ (void)openBookmark:(NSDictionary *)bookmark;
+ (void)deleteBookmark:(NSDictionary *)bookmark completion:(void (^)(BOOL success))completion;

+ (void)openItineraryFromURL:(NSURL*)url;

+ (BOOL)hasBookmarks;

+ (NSArray*)getBookmarksArray;

+ (NSData*)dataFromItinerary:(Itinerary*)itinerary;
+ (Itinerary*)itineraryFromData:(NSData*)itineraryData;

+ (NSURL*)outgoingSharingUrlForItinerary:(Itinerary*)itinerary;

@end
