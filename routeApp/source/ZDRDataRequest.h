//
//  ZDRDataRequest.h
//  routeApp
//
//  Created by Ziga Dolar on 27/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^errorBlock)(NSError *);
typedef void(^successBlock)(NSDictionary *);

@interface ZDRDataRequest : NSObject

    @property (nonatomic, strong) NSURL *fetchingURL;
    @property (nonatomic, strong) NSURLSessionTask *fetchingSessionTask;
    @property (nonatomic, strong) NSData *fetchedData;

- (void)fetchDataAtURL:(NSURL*)url withSuccess:(successBlock)aSuccessHandler error:(errorBlock)anErrorHandler;
- (void)cancel;

@end
