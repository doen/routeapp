//
//  Waypoint.h
//  routeApp
//
//  Created by Ziga Dolar on 26/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLLocation;

@interface Waypoint : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *address;
@property (nonatomic) BOOL droppedPin;

@property (nonatomic) BOOL added;

@property (readonly) CLLocation *location;

+ (instancetype)waypointWithLocation:(CLLocation *)location title:(NSString*)title address:(NSString*)address;
+ (instancetype)waypointWithDictionary:(NSDictionary*)dictionary;
+ (instancetype)waypointFromDroppedPinAtLocation:(CLLocation *)location;

@end
