//
//  ZDRWaypointTableViewCell.m
//  routeApp
//
//  Created by Ziga Dolar on 25/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRWaypointTableViewCell.h"

@interface ZDRWaypointTableViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;

@end

@implementation ZDRWaypointTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTitleText:(NSString *)titleText
{
    _titleText = titleText;
    
    self.titleLabel.text = titleText;
}

- (void)setAddressText:(NSString *)addressText
{
    _addressText = addressText;
    
    self.addressLabel.text = addressText;
}

@end
