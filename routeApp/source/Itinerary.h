//
//  Itinerary.h
//  routeApp
//
//  Created by Ziga Dolar on 26/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Waypoint;

typedef enum {
    kTransportModeCar,
    kTransportModePedestrian,
    kTransportPublicTransport
} TransportMode;

@interface Itinerary : NSObject

@property (readonly) NSMutableArray *waypoints;
@property (readonly) TransportMode modeOfTransport;

- (void)addWaypointToItinerary:(Waypoint *)waypoint completion:(void (^)(BOOL success))completion;
- (void)removeWaypointAtIndex:(NSUInteger)atIndex;
- (void)moveWaypointFromIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex;

- (void)changeTransportMode:(TransportMode)newMode withChange:(void (^)(BOOL))changeBlock;

- (BOOL)hasWaypoint:(Waypoint*)waypoint;

@end
