//
//  Itinerary.m
//  routeApp
//
//  Created by Ziga Dolar on 26/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "Itinerary.h"

#import "Waypoint.h"

@interface Itinerary()

@property (readwrite) NSMutableArray *waypoints;
@property (readwrite) TransportMode modeOfTransport;

@end

@implementation Itinerary

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.waypoints = [NSMutableArray new];
        self.modeOfTransport = kTransportModeCar;
    }
    return self;
}

- (void)addWaypointToItinerary:(Waypoint *)waypoint completion:(void (^)(BOOL))completion
{
    BOOL success = NO;
    
    if (![self.waypoints containsObject:waypoint]) {
        [self.waypoints addObject:waypoint];
        success = YES;
    }
    
    if (completion) {
        completion(success);
    }
}

- (void)removeWaypointAtIndex:(NSUInteger)atIndex
{
    [self.waypoints removeObjectAtIndex:atIndex];
}

- (void)moveWaypointFromIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex
{
    Waypoint *waypoint = [self.waypoints objectAtIndex:fromIndex];
    [self.waypoints removeObject:waypoint];
    [self.waypoints insertObject:waypoint atIndex:toIndex];
}

- (void)changeTransportMode:(TransportMode)newMode withChange:(void (^)(BOOL))changeBlock
{
    bool didChange = (self.modeOfTransport != newMode);
    
    self.modeOfTransport = newMode;
    if (changeBlock) {
        changeBlock(didChange);
    }
    
}

- (BOOL)hasWaypoint:(Waypoint *)waypoint
{
    return [self.waypoints containsObject:waypoint];
}

# pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.waypoints       = [aDecoder decodeObjectForKey:@"waypoints"    ];
        self.modeOfTransport = (TransportMode)[aDecoder decodeIntegerForKey:@"transportMode"];
        
        return self;
    }
    return nil;
}

- (void)encodeWithCoder:(NSCoder *)anEncoder {
    [anEncoder encodeObject:self.waypoints forKey:@"waypoints"];
    [anEncoder encodeInteger:self.modeOfTransport forKey:@"transportMode"];
}

@end
