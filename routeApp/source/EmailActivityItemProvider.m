//
//  EmailActivityItemProvider.m
//  routeApp
//
//  Created by Ziga Dolar on 29/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "EmailActivityItemProvider.h"

#import "ZDRItineraryManager.h"

@implementation EmailActivityItemProvider

- (NSString *)activityViewController:(UIActivityViewController *)activityViewController subjectForActivityType:(NSString *)activityType
{
    return NSLocalizedString(@"share_email_subject", @"Share route email subject");//@"Here's a route";
}

-(id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    NSURL *itineraryURL = [ZDRItineraryManager outgoingSharingUrlForItinerary:[ZDRItineraryManager sharedManager].currentItinerary];
    
    NSString *emailBodyTemplate = NSLocalizedString(@"share_email_body", @"Share route email body template"); //@"<html><body>Hi! <br/><br/>Here's the route: <br/><br/><br/><a href=\"%@\">Open route in RouteApp</a></body></html>";
    
    return [NSString stringWithFormat:emailBodyTemplate, [itineraryURL absoluteString]];
}

- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return @"";
}

@end
