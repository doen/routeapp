//
//  Route.h
//  routeApp
//
//  Created by Ziga Dolar on 26/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MKPolyline, CLLocation;

@interface Route : NSObject

@property (readonly) MKPolyline *routeLine;
@property (readonly) NSNumber *duration;

+ (instancetype)routeWithDictionary:(NSDictionary*)dictionary;

@end


@interface RoutePoint : NSObject

@property (nonatomic, strong) CLLocation *location;

+ (instancetype)routePointWithDictionary:(NSDictionary*)dictionary;

@end