//
//  ZDRBookmarkTableViewCell.m
//  routeApp
//
//  Created by Ziga Dolar on 29/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRBookmarkTableViewCell.h"

@implementation ZDRBookmarkTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        [self configureViews];
    }
    return self;
}

- (void)configureViews
{
    self.detailTextLabel.textColor = [UIColor grayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
