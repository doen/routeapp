//
//  ZDRURLHandler.h
//  routeApp
//
//  Created by Ziga Dolar on 29/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIApplication;

@interface ZDRURLHandler : NSObject

+ (BOOL)openURLscheme:(NSURL*)url sourceApplication:(NSString*)sourceApplication withAnnotation:(id)annotation application:(UIApplication*)application;

@end
