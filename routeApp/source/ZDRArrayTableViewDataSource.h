//
//  ZDRArrayTableViewDataSource.h
//  routeApp
//
//  Created by Žiga Dolar on 27/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^TableViewCellConfigurationBlock)(id cell, id item);

@interface ZDRArrayTableViewDataSource : NSObject <UITableViewDataSource>

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, copy)   NSString *cellIdentifier;
@property (nonatomic, copy)   TableViewCellConfigurationBlock cellConfigurationBlock;

- (id)initWithItemsArray:(NSArray *)anArray
     cellIdentifier:(NSString *)aCellIdentifier
 cellConfigurationBlock:(TableViewCellConfigurationBlock)aCellConfigurationBlock;

- (id)itemAtIndexPath:(NSIndexPath *)indexPath;

@end