//
//  ZDRRequestHelper.h
//  routeApp
//
//  Created by Ziga Dolar on 27/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Itinerary;

@interface ZDRRequestHelper : NSObject

+ (NSURL*)searchURLForSearchString:(NSString*)searchString;

+ (NSURL*)routeURLWithItinerary:(Itinerary*)itinerary;

@end
