//
//  EmailActivityItemProvider.h
//  routeApp
//
//  Created by Ziga Dolar on 29/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailActivityItemProvider : UIActivityItemProvider

@end
