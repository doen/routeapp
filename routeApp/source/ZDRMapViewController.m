//
//  ZDRMapViewController.m
//  routeApp
//
//  Created by Ziga Dolar on 25/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRMapViewController.h"

#import <MapKit/MapKit.h>

#import "ZDRMapPointAnnotation.h"

#import "ZDRItineraryManager.h"
#import "Itinerary.h"
#import "Waypoint.h"

#import "Route.h"

#import "ZDRDataRequest.h"
#import "ZDRRequestHelper.h"

#import "ZDRLocationManager.h"

@interface ZDRMapViewController () <ZDRItineraryManagerDelegate, MKMapViewDelegate>

@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, weak) IBOutlet UILabel *durationLabel;

@property (nonatomic, weak) IBOutlet UIButton *carButton;
@property (nonatomic, weak) IBOutlet UIButton *walkButton;
@property (nonatomic, weak) IBOutlet UIButton *trainButton;

@property (nonatomic, retain) MKPolyline *routeLine; //your line
@property (nonatomic, retain) MKPolylineView *routeLineView; //overlay view

@property (strong, nonatomic) ZDRDataRequest *routeRequest;

@end

@implementation ZDRMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [ZDRLocationManager sharedManager].map = self.mapView;
    self.mapView.delegate = self;
    
    [self configureMapViewGestureRecognizer];
    
    self.durationLabel.layer.shadowRadius = 5;
    [self configureDurationForRoute:nil];
    
    [self configureButtons];
    
    [ZDRItineraryManager sharedManager].delegate = self;
}

- (void)configureMapViewGestureRecognizer
{
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    [self.mapView addGestureRecognizer:longPressGesture];
}

- (void)handleLongPressGesture:(UIGestureRecognizer*)sender
{
    if (sender.state == UIGestureRecognizerStateEnded || sender.state == UIGestureRecognizerStateChanged)
    {
        return;
    }
    else
    {
        CGPoint point = [sender locationInView:self.mapView];
        CLLocationCoordinate2D coordinates = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:coordinates.latitude longitude:coordinates.longitude];
        
        Waypoint *dropWaypoint = [Waypoint waypointFromDroppedPinAtLocation:location];
        [[ZDRItineraryManager sharedManager] addWaypointToItinerary:dropWaypoint];
    }
}

-(MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    if(overlay == self.routeLine)
    {
        if(self.routeLineView == nil)
        {
            self.routeLineView = [[MKPolylineView alloc] initWithPolyline:self.routeLine];
            self.routeLineView.fillColor = [UIColor redColor];
            self.routeLineView.strokeColor = [UIColor redColor];
            self.routeLineView.lineWidth = 5;
            
        }
        
        return self.routeLineView;
    }
    
    return nil;
}

- (void)currentItineraryChanged
{
    [self configureButtonStates];
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    for (Waypoint *waypoint in [[ZDRItineraryManager sharedManager] currentItinerary].waypoints) {
        // Add an annotation
        ZDRMapPointAnnotation *point = [ZDRMapPointAnnotation new];
        point.waypoint = waypoint;
        
        [self.mapView addAnnotation:point];
    }
    
    [self updateRoute];
}

- (void)updateRoute
{
    
    if (self.routeRequest) {
        [self.routeRequest cancel];
        self.routeRequest = nil;
    }
    
    if ([ZDRItineraryManager sharedManager].currentItinerary.waypoints.count > 1) {
        
        NSURL *routeURL = [ZDRRequestHelper routeURLWithItinerary:[ZDRItineraryManager sharedManager].currentItinerary];
        
        self.routeRequest = [ZDRDataRequest new];
        [self.routeRequest fetchDataAtURL:routeURL
                              withSuccess:^(NSDictionary *dictionary) {
                                  Route *newRoute = [Route routeWithDictionary:dictionary];
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [self configureDurationForRoute:newRoute];
                                      [self drawRoute:newRoute];
                                  });
                              } error:^(NSError *error) {
//                                  NSLog(@"error: %@", error);
                              }];
    } else {
        [self configureDurationForRoute:nil];
        [self drawRoute:nil];
        
        Waypoint *waypoint = [ZDRItineraryManager sharedManager].currentItinerary.waypoints.firstObject;
        
        if (waypoint) {
            [self centerMapOnLocation:waypoint.location];
        }
    }
}

- (void)configureDurationForRoute:(Route*)route
{
    if (!route) {
        self.durationLabel.text = @"";
    } else {
        self.durationLabel.text = [self stringForDuration:route.duration];
    }
    
}

- (NSString*)stringForDuration:(NSNumber*)duration
{
    NSInteger durationInSeconds = [duration integerValue];
    
    NSInteger hours   = durationInSeconds / 3600;
    NSInteger minutes = (durationInSeconds % 3600) / 60;
    NSInteger seconds = durationInSeconds % 60;
    
    NSString *time = (hours > 0) ? [NSString stringWithFormat:@"%lih ", (long)hours] : @"";
    
    time = (hours > 0 || minutes > 0) ? [time stringByAppendingString:[NSString stringWithFormat:@"%lim ", (long)minutes]] : time;
    
    time = (seconds > 0) ? [time stringByAppendingString:[NSString stringWithFormat:@"%lis",(long)seconds]] : time;
    
    if (hours > 23) {
        NSInteger days = durationInSeconds / 3600 / 24;
        time = [NSString stringWithFormat:@"%lid", (long)days];
    }
    
    return time;
}

- (void)drawRoute:(Route*)route
{
    [self.mapView removeOverlays:self.mapView.overlays];
    self.routeLineView = nil;
    
    self.routeLine = route.routeLine;
    
    if (self.routeLine) {
        MKCoordinateRegion region = MKCoordinateRegionForMapRect([self.routeLine boundingMapRect]);
        region.span.latitudeDelta *= 1.2;
        region.span.longitudeDelta *= 1.2;
        [self.mapView setRegion:region animated:YES];
        [self.mapView addOverlay:self.routeLine];
    }
}

- (void)centerMapOnLocation:(CLLocation*)location
{
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D coordinates = location.coordinate;
    region.span = span;
    region.center = coordinates;
    [self.mapView setRegion:region animated:YES];
}

#pragma mark - actions

- (void)configureButtons
{
    [self.carButton setTitle:@"C" forState:UIControlStateNormal];
    [self.carButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
    
    [self.walkButton setTitle:@"W" forState:UIControlStateNormal];
    [self.walkButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
    
    [self.trainButton setTitle:@"PT" forState:UIControlStateNormal];
    [self.trainButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
    
    [self configureButtonStates];
}

- (void)configureButtonStates
{
    TransportMode mode = [ZDRItineraryManager sharedManager].currentItinerary.modeOfTransport;
    
    self.carButton.selected   = (mode == kTransportModeCar);
    self.walkButton.selected  = (mode == kTransportModePedestrian);
    self.trainButton.selected = (mode == kTransportPublicTransport);
}

- (IBAction)carButtonPressed:(id)sender
{
    [[ZDRItineraryManager sharedManager] setTransportModeCar];
}

- (IBAction)publicTransportButtonPressed:(id)sender
{
    [[ZDRItineraryManager sharedManager] setTransportModePublicTransport];
}

- (IBAction)walkingButtonPressed:(id)sender
{
    [[ZDRItineraryManager sharedManager] setTransportModePedestrian];
}

@end
