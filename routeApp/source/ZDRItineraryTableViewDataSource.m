//
//  ZDRItineraryTableViewDataSource.m
//  routeApp
//
//  Created by Žiga Dolar on 27/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRItineraryTableViewDataSource.h"

#import "ZDRWaypointTableViewCell.h"

#import "ZDRItineraryManager.h"

#import "Waypoint.h"
#import "Itinerary.h"

@implementation ZDRItineraryTableViewDataSource

- (instancetype)init
{
    self = [super init];
    if (self) {
        void (^cellConfiguration)(ZDRWaypointTableViewCell *, Waypoint *) = ^(ZDRWaypointTableViewCell *cell, Waypoint *waypoint){
            cell.showsReorderControl = YES;
            
            // Configure the cell...
            cell.titleText   = waypoint.title;
            cell.addressText = waypoint.address;
        };
        
        self.cellIdentifier = @"WaypointCell";
        self.cellConfigurationBlock = cellConfiguration;
    }
    return self;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath
{
    return [ZDRItineraryManager sharedManager].currentItinerary.waypoints[(NSUInteger) indexPath.row];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [ZDRItineraryManager sharedManager].currentItinerary.waypoints.count;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        
        [[ZDRItineraryManager sharedManager] removeWaypointAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    [[ZDRItineraryManager sharedManager] moveWaypointFromIndex:fromIndexPath.row toIndex:toIndexPath.row];
}

@end
