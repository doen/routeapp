//
//  ZDRWaypointTableViewDataSource.h
//  routeApp
//
//  Created by Žiga Dolar on 27/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRArrayTableViewDataSource.h"

@interface ZDRWaypointTableViewDataSource : ZDRArrayTableViewDataSource

- (instancetype)initWithItemsArray:(NSArray*)anArray;

@end
