//
//  ZDRItineraryTableViewController.m
//  routeApp
//
//  Created by Ziga Dolar on 25/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRItineraryTableViewController.h"

#import "ZDRPresenter.h"
#import "ZDRWaypointSearchNavigationController.h"
#import "ZDRWaypointSearchTableViewController.h"

#import "ZDRItineraryBookmarkNavigationController.h"

#import "ZDRItineraryTableViewDataSource.h"

#import "ZDRWaypointTableViewCell.h"

#import "ZDRItineraryManager.h"
#import "Itinerary.h"
#import "Waypoint.h"

#import "EmailActivityItemProvider.h"

@interface ZDRItineraryTableViewController ()

@property (nonatomic, strong) Itinerary *itinerary;
@property (nonatomic, strong) ZDRItineraryTableViewDataSource *itineraryDataSource;

@end

@implementation ZDRItineraryTableViewController

# pragma mark - configuration

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.navigationController setToolbarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addButtonPressed:)];
    
    self.navigationItem.title = NSLocalizedString(@"Itinerary", @"Itinerary navigation controller title") ;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    [self configureTableView];
    [self configureBottomBar];
}

- (void)configureTableView
{
    [self.tableView registerNib:[UINib nibWithNibName:@"ZDRWaypointTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"WaypointCell"];
    self.tableView.rowHeight = 60;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    self.tableView.allowsSelection = NO;
    self.tableView.allowsSelectionDuringEditing = NO;
    
    self.itineraryDataSource = [ZDRItineraryTableViewDataSource new];
    
    self.tableView.dataSource = self.itineraryDataSource;
}

- (void)configureBottomBar
{
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareButtonPressed)];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *bookmarksButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(bookmarksButtonPressed:)];
    
    UIBarButtonItem *endFlexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    endFlexibleSpace.width = 1;
    
    self.toolbarItems = @[shareButton, flexibleSpace, bookmarksButton, endFlexibleSpace];
}

# pragma mark - actions

- (void)addButtonPressed:(id)sender
{
    bool presentModally = false;
    
    if (presentModally) {
        ZDRWaypointSearchNavigationController *searchViewController = [ZDRWaypointSearchNavigationController new];
        searchViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        
        [[ZDRPresenter topmostViewController] presentViewController:searchViewController animated:YES completion:nil];
    } else {
        ZDRWaypointSearchTableViewController *searchViewController = [ZDRWaypointSearchTableViewController new];
        
        bool animated = YES;
        
        [self.navigationController pushViewController:searchViewController animated:animated];
        [self.navigationController setToolbarHidden:YES animated:animated];
    }
}

- (void)bookmarksButtonPressed:(id)sender
{
    UIAlertController *view = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    {
        UIAlertAction *addBookmark = [UIAlertAction actionWithTitle:NSLocalizedString(@"Add to Bookmarks", @"Bookmarks action sheet 'add' title")
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction *action) {
                                                                [self addBookmarkButtonPressed];
                                                                [view dismissViewControllerAnimated:YES completion:nil];
                                                            }];
        [view addAction:addBookmark];
    }

    if ([ZDRItineraryManager hasBookmarks]) {
        UIAlertAction *openBookmark = [UIAlertAction actionWithTitle:NSLocalizedString(@"Open Boookmark", @"Bookmarks action sheet 'open' title")
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action) {
                                                                 [self openBookmarkButtonPressed];
                                                                 [view dismissViewControllerAnimated:YES completion:nil];
                                                             }];
        [view addAction:openBookmark];
    }
    
    {
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Bookmarks action sheet 'cancel' button title")
                                                         style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action) {
                                                           [view dismissViewControllerAnimated:YES completion:nil];
                                                           
                                                       }];
        [view addAction:cancel];
    }
    
    [self presentViewController:view animated:YES completion:nil];
}

- (void)addBookmarkButtonPressed
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:NSLocalizedString(@"Enter bookmark name", @"Bookmark name prompt")
                               message:nil
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"Bookmarks save prompt 'ok'") style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action){
                                                   UITextField *textField = alert.textFields[0];
                                                   [ZDRItineraryManager addCurrentItineraryToBookmarksWithName:textField.text];
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Bookmarks save prompt 'cancel'") style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"ex.: My Route", @"Placeholder Bookmark name");
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)openBookmarkButtonPressed
{
    ZDRItineraryBookmarkNavigationController *bookmarkViewController = [ZDRItineraryBookmarkNavigationController new];
    bookmarkViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [[ZDRPresenter topmostViewController] presentViewController:bookmarkViewController animated:YES completion:nil];
}

- (void)shareButtonPressed
{
    
    EmailActivityItemProvider *shareRouteProvider = [EmailActivityItemProvider new];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:@[shareRouteProvider] applicationActivities:nil];
    
    controller.excludedActivityTypes = @[UIActivityTypePrint,
                                         UIActivityTypeCopyToPasteboard,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypeAirDrop,
                                         UIActivityTypeMessage,
                                         UIActivityTypePostToFacebook,
                                         UIActivityTypePostToTwitter,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypePostToWeibo];
    
    [self presentViewController:controller animated:YES completion:nil];
}

@end
