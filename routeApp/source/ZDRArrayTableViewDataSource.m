//
//  ZDRArrayTableViewDataSource.m
//  routeApp
//
//  Created by Žiga Dolar on 27/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRArrayTableViewDataSource.h"

@implementation ZDRArrayTableViewDataSource

- (id)initWithItemsArray:(NSArray *)anArray cellIdentifier:(NSString *)aCellIdentifier cellConfigurationBlock:(TableViewCellConfigurationBlock)aCellConfigurationBlock
{
    self = [super init];
    if (self) {
        self.items = anArray;
        self.cellIdentifier = aCellIdentifier;
        self.cellConfigurationBlock = [aCellConfigurationBlock copy];
    }
    return self;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.items[(NSUInteger) indexPath.row];
}


#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier
                                                            forIndexPath:indexPath];
    id item = [self itemAtIndexPath:indexPath];
    self.cellConfigurationBlock(cell, item);
    return cell;
}

@end
