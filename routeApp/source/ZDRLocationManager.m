//
//  ZDRLocationManager.m
//  routeApp
//
//  Created by Ziga Dolar on 28/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRLocationManager.h"

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ZDRLocationManager () <CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
}

@end

@implementation ZDRLocationManager

+ (instancetype)sharedManager
{
    static dispatch_once_t onceToken;
    static id sharedManager;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = 10;// kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
}

- (NSString *)currentCoordinatesString
{
    CLLocationCoordinate2D coordinates = self.map.centerCoordinate;
    
    return [NSString stringWithFormat:@"%f,%f", coordinates.latitude, coordinates.longitude];
}

@end
