//
//  ZDRItineraryTableViewController.h
//  routeApp
//
//  Created by Ziga Dolar on 25/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Waypoint;

@protocol ZDRItineraryTableViewDelegate <NSObject>

- (void)didSelectWaypoint:(Waypoint*)waypoint;

@end

@interface ZDRItineraryTableViewController : UITableViewController

@property (nonatomic, weak) id<ZDRItineraryTableViewDelegate> itineraryTableViewDelegate;

@end
