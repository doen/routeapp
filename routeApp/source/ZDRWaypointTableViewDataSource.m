//
//  ZDRWaypointTableViewDataSource.m
//  routeApp
//
//  Created by Žiga Dolar on 27/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRWaypointTableViewDataSource.h"

#import "ZDRWaypointTableViewCell.h"
#import "Waypoint.h"

@implementation ZDRWaypointTableViewDataSource

- (instancetype)initWithItemsArray:(NSArray *)anArray
{
    self = [super initWithItemsArray:anArray cellIdentifier:@"WaypointCell" cellConfigurationBlock:^(ZDRWaypointTableViewCell *cell, Waypoint *waypoint){
        // Configure the cell...
        cell.titleText   = waypoint.title;
        cell.addressText = waypoint.address;
        
        cell.accessoryType = (waypoint.added) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    }];
    return self;
}

@end
