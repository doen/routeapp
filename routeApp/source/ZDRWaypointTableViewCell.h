//
//  ZDRWaypointTableViewCell.h
//  routeApp
//
//  Created by Ziga Dolar on 25/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZDRWaypointTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *titleText;
@property (nonatomic, strong) NSString *addressText;

@end
