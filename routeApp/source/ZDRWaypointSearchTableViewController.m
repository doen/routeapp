//
//  ZDRWaypointSearchTableViewController.m
//  routeApp
//
//  Created by Ziga Dolar on 25/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRWaypointSearchTableViewController.h"

#import "ZDRWaypointTableViewCell.h"

#import "ZDRWaypointTableViewDataSource.h"

#import "ZDRItineraryManager.h"
#import "Itinerary.h"
#import "Waypoint.h"

#import "ZDRDataRequest.h"
#import "ZDRRequestHelper.h"

@interface ZDRWaypointSearchTableViewController () <UISearchBarDelegate>

@property (nonatomic, strong) ZDRWaypointTableViewDataSource *waypointsDataSource;

@property (strong, nonatomic) UISearchBar *searchBar;

@property (strong, nonatomic) NSArray *waypoints;

@property (strong, nonatomic) ZDRDataRequest *searchRequest;

@end

@implementation ZDRWaypointSearchTableViewController

# pragma mark - configuration

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.presentingViewController) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                                               target:self
                                                                                               action:@selector(cancel)];
    }
    
    // configure search bar in navigation bar
    [self configureSearchBar];

    [self configureTableView];
}

- (void)configureTableView
{
    [self.tableView registerNib:[UINib nibWithNibName:@"ZDRWaypointTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"WaypointCell"];
    self.tableView.rowHeight = 60;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    self.waypointsDataSource = [[ZDRWaypointTableViewDataSource alloc] initWithItemsArray:self.waypoints];
    self.tableView.dataSource = self.waypointsDataSource;
}

- (void)configureSearchBar
{
    self.searchBar = [[UISearchBar alloc] initWithFrame:self.navigationController.navigationBar.bounds];
    self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    
    self.searchBar.delegate = self;
    
    self.navigationItem.titleView = self.searchBar;
}


#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Waypoint *waypoint = (Waypoint*)[self.waypointsDataSource itemAtIndexPath:indexPath];
    
    if (waypoint.added) {
        [[ZDRItineraryManager sharedManager] removeWaypointFromItinerary:waypoint];
        waypoint.added = NO;
    } else {
        [[ZDRItineraryManager sharedManager] addWaypointToItinerary:waypoint];
        waypoint.added = YES;
    }
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


# pragma mark - scroll view delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self searchShouldLoseFirstResponder];
}

- (void)searchShouldLoseFirstResponder
{
    [self.view endEditing:YES];
    [self.searchBar resignFirstResponder];
}

# pragma mark - search bar delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = YES;

}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length > 0) {
        [self fetchWaypointsForSearchString:searchText];
    } else {
        
        if (self.searchRequest) {
            [self.searchRequest cancel];
            self.searchRequest = nil;
        }
        
        self.waypoints = @[];
        [self updateTableView];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = @"";
    [self.searchBar endEditing:YES];
}

#pragma mark - 

- (void)fetchWaypointsForSearchString:(NSString*)string
{
    NSURL *searchURL = [ZDRRequestHelper searchURLForSearchString:string];
    
    if (self.searchRequest) {
        [self.searchRequest cancel];
        self.searchRequest = nil;
    }
    
    self.searchRequest = [ZDRDataRequest new];
    [self.searchRequest fetchDataAtURL:searchURL
                           withSuccess:^(NSDictionary *dictionary) {
                               [self reloadWaypointsFromFetchedDictionary:dictionary];
                           } error:^(NSError *error) {
//                               NSLog(@"error: %@", error);
                           }];
}

- (void)reloadWaypointsFromFetchedDictionary:(NSDictionary*)dictionary
{
    NSMutableArray *waypoints = [NSMutableArray new];
    
    NSArray *items = dictionary[@"results"][@"items"];
    
    for (NSDictionary *item in items) {
        Waypoint *waypoint = [Waypoint waypointWithDictionary:item];
        
        waypoint.added = [[ZDRItineraryManager sharedManager].currentItinerary hasWaypoint:waypoint];
        
        [waypoints addObject:waypoint];
    }
    
    self.waypoints = [waypoints copy];
    [self updateTableView];
}

- (void)updateTableView
{
    self.waypointsDataSource.items = self.waypoints;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    });
}

#pragma mark - Actions

- (void)cancel
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
