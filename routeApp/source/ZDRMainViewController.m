//
//  ZDRMainViewController.m
//  routeApp
//
//  Created by Ziga Dolar on 25/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRMainViewController.h"

// View Controllers
#import "ZDRItineraryNavigationController.h"
#import "ZDRMapViewController.h"

@interface ZDRMainViewController ()

// IBOutlets
// Constraints
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *itineraryContainerDistance;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *itineraryMaxWidthConstraint;

// Views
@property (nonatomic, weak) IBOutlet UIView *mapViewContainer;
@property (nonatomic, weak) IBOutlet UIView *itineraryViewContainer;

// Buttons
@property (nonatomic, weak) IBOutlet UIButton *toggleItineraryButton;

// View Controllers
@property (nonatomic, strong) ZDRItineraryNavigationController *itineraryVC;
@property (nonatomic, strong) ZDRMapViewController *mapVC;

@end

@implementation ZDRMainViewController

# pragma mark - configuration

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.itineraryMaxWidthConstraint.constant = MIN(MIN(CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)), 414);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // configure UI elements
    [self configureViews];
    
    [self configureChildViewControllers];
}

- (void)configureViews
{
    self.itineraryViewContainer.backgroundColor = [UIColor whiteColor];
    
    self.itineraryViewContainer.layer.masksToBounds = NO;
    self.itineraryViewContainer.layer.shadowOffset = CGSizeMake(-2, 0);
    self.itineraryViewContainer.layer.shadowRadius = 4;
    self.itineraryViewContainer.layer.shadowOpacity = 0.25;
    
    
    self.toggleItineraryButton.backgroundColor = [UIColor darkGrayColor];
    [self.toggleItineraryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    // set button titles for states
    [self.toggleItineraryButton setTitle:@"<" forState:UIControlStateNormal];
    [self.toggleItineraryButton setTitle:@">" forState:UIControlStateSelected];
    
    [self configureToggleItineraryButtonState];
}

- (void)configureChildViewControllers
{
    // add itinerary view controller as child view controller and add it to the view
    self.itineraryVC = [ZDRItineraryNavigationController new];
    [self addChildViewController:self.itineraryVC];
    
    self.itineraryVC.view.frame = self.itineraryViewContainer.bounds;
    [self.itineraryViewContainer addSubview:self.itineraryVC.view];
    
    // add map view controller as child view controller and add it to the view
    self.mapVC = [ZDRMapViewController new];
    [self addChildViewController:self.mapVC];
    
    self.mapVC.view.frame = self.mapViewContainer.bounds;
    [self.mapViewContainer addSubview:self.mapVC.view];
}

# pragma mark - actions

- (IBAction)toggleItineraryButtonPressed:(id)sender
{
    self.itineraryContainerDistance.constant = (self.itineraryContainerDistance.constant > 0) ? 0 : 2000;
    
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
        [self configureToggleItineraryButtonState];
    } completion:^(BOOL finished) {
        if (self.itineraryContainerDistance.constant > 0) {
            [self.itineraryVC popToRootViewControllerAnimated:NO];
        }
    }];
}

# pragma mark - internal

- (void)configureToggleItineraryButtonState
{
    self.toggleItineraryButton.selected = self.itineraryContainerDistance.constant == 0;
}

@end
