//
//  ZDRDataRequest.m
//  routeApp
//
//  Created by Ziga Dolar on 27/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRDataRequest.h"

@interface ZDRDataRequest()

@property (nonatomic, copy) errorBlock   errorHandler;
@property (nonatomic, copy) successBlock successHandler;

@end

@implementation ZDRDataRequest

- (void)fetchDataAtURL:(NSURL *)url withSuccess:(successBlock)aSuccessHandler error:(errorBlock)anErrorHandler
{
    self.fetchingURL    = url;
    self.successHandler = aSuccessHandler;
    self.errorHandler   = anErrorHandler;
    
    NSURLSession *session = [NSURLSession sharedSession];
    self.fetchingSessionTask = [session dataTaskWithURL:self.fetchingURL
                                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                {
                                    if (!error) {
                                        if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                            
                                            if (httpResponse.statusCode == 200) {
                                                if (aSuccessHandler) {
                                                    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                 options:0
                                                                                                                   error:nil];
                                                    aSuccessHandler(responseDict);
                                                }
                                            }
                                            
                                        }
                                    } else {
                                        if (anErrorHandler) {
                                            anErrorHandler(error);
                                        }
                                    }
                                    
                                }];
    
    [self.fetchingSessionTask resume];

}

- (void)cancel
{
    [self.fetchingSessionTask cancel];
    self.fetchingURL = nil;
    self.fetchingSessionTask = nil;
    self.fetchedData = nil;
}

@end
