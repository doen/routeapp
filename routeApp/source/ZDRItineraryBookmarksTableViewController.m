//
//  ZDRItineraryBookmarksTableViewController.m
//  routeApp
//
//  Created by Ziga Dolar on 29/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRItineraryBookmarksTableViewController.h"
#import "ZDRBookmarkTableViewCell.h"

#import "ZDRItineraryManager.h"

@interface ZDRItineraryBookmarksTableViewController ()

@property (nonatomic, strong) NSArray *bookmarksArray;

@end

@implementation ZDRItineraryBookmarksTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.presentingViewController) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                                               target:self
                                                                                               action:@selector(close)];
        self.navigationItem.leftBarButtonItem = self.editButtonItem;
    } else {
        self.navigationItem.rightBarButtonItem = self.editButtonItem;
    }
    
    self.navigationItem.title = NSLocalizedString(@"Bookmarks", @"Bookmarks navigation controller title") ;
    
    [self configureBookmarksArray];
    
    [self configureTableView];
}

- (void)configureBookmarksArray
{
    self.bookmarksArray = [[ZDRItineraryManager getBookmarksArray] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"bookmarkDate" ascending:NO]]];
}

- (void)configureTableView
{
    [self.tableView registerClass:[ZDRBookmarkTableViewCell class] forCellReuseIdentifier:@"bookmarkCell"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    self.tableView.allowsSelectionDuringEditing = NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *bookmark = self.bookmarksArray[indexPath.row];
    [ZDRItineraryManager openBookmark:bookmark];
    
    [self close];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.bookmarksArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *reuseIdentifier = @"bookmarkCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    NSDictionary *bookmark = self.bookmarksArray[indexPath.row];
    
    NSString *bookmarkName = bookmark[@"bookmarkName"];
    NSDate *bookmarkDate = bookmark[@"bookmarkDate"];
    
    cell.textLabel.text = bookmarkName;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    
    cell.detailTextLabel.text = [dateFormatter stringFromDate:bookmarkDate];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSDictionary *bookmarkToDelete = self.bookmarksArray[indexPath.row];
        
        [ZDRItineraryManager deleteBookmark:bookmarkToDelete completion:^(BOOL success) {
            if (success) {
                [self configureBookmarksArray];
                
                // Delete the row from the data source
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

#pragma mark - Actions

- (void)close
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
