//
//  ZDRItineraryBookmarkNavigationController.m
//  routeApp
//
//  Created by Ziga Dolar on 29/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRItineraryBookmarkNavigationController.h"

#import "ZDRItineraryBookmarksTableViewController.h"

@interface ZDRItineraryBookmarkNavigationController ()

@end

@implementation ZDRItineraryBookmarkNavigationController

- (instancetype)init
{
    ZDRItineraryBookmarksTableViewController *itineraryBookmarksTableViewController = [ZDRItineraryBookmarksTableViewController new];
    
    self = [super initWithRootViewController:itineraryBookmarksTableViewController];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
