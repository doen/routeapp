//
//  Route.m
//  routeApp
//
//  Created by Ziga Dolar on 26/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "Route.h"

#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface Route ()

@property (nonatomic, strong) NSMutableArray *routePoints;
@property (readwrite) NSNumber *duration;

@end

@implementation Route

+ (instancetype)routeWithDictionary:(NSDictionary *)dictionary
{
    return [[self alloc] initWithDictionary:dictionary];
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        
        NSDictionary *routeDict = [dictionary[@"response"][@"route"] firstObject];
        
        self.duration = routeDict[@"summary"][@"baseTime"];
        
        NSArray *legPointsArray = routeDict[@"leg"];
        
        self.routePoints = [NSMutableArray new];
        
        for (NSDictionary *legPoint in legPointsArray) {
            NSArray *maneuverPoints = legPoint[@"maneuver"];
            
            for (NSDictionary *maneuverPoint in maneuverPoints) {
                [self.routePoints addObject:[RoutePoint routePointWithDictionary:maneuverPoint]];
            }
        }
    }
    return self;
}

- (MKPolyline *)routeLine
{
    if (self.routePoints.count < 2) {
        return nil;
    }
    
    CLLocationCoordinate2D coordinateArray[self.routePoints.count];
    
    for (int i = 0; i < self.routePoints.count; i++) {
        RoutePoint *point = self.routePoints[i];
        coordinateArray[i] = point.location.coordinate;
    }
    
    return [MKPolyline polylineWithCoordinates:coordinateArray count:self.routePoints.count];
}

@end


@implementation RoutePoint

+ (instancetype)routePointWithDictionary:(NSDictionary *)dictionary
{
    return [[self alloc] initWithDictionary:dictionary];
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        
        NSDictionary *position = dictionary[@"position"];
        
        NSNumber *latitude  = position[@"latitude"];
        NSNumber *longitude = position[@"longitude"];
        
        self.location = [[CLLocation alloc] initWithLatitude:latitude.doubleValue longitude:longitude.doubleValue];
    }
    return self;
}

@end