//
//  ZDRMapPointAnnotation.h
//  routeApp
//
//  Created by Ziga Dolar on 26/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <MapKit/MapKit.h>

@class Waypoint;

@interface ZDRMapPointAnnotation : MKPointAnnotation

@property (nonatomic, strong) Waypoint *waypoint;

@end
