//
//  ZDRItineraryManager.m
//  routeApp
//
//  Created by Ziga Dolar on 26/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRItineraryManager.h"

#import <UIKit/UIKit.h>

#import "Itinerary.h"
#import "Waypoint.h"

@implementation ZDRItineraryManager

+ (instancetype)sharedManager
{
    static dispatch_once_t onceToken;
    static id sharedManager;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.currentItinerary = [Itinerary new];
    }
    return self;
}

- (void)addWaypointToItinerary:(Waypoint *)waypoint
{
    __weak typeof(self) weakSelf = self;
    
    [self.currentItinerary addWaypointToItinerary:waypoint completion:^(BOOL success) {
        if (weakSelf.delegate) {
            [weakSelf.delegate currentItineraryChanged];
        }
    }];
}

- (void)removeWaypointAtIndex:(NSUInteger)atIndex
{
    [self.currentItinerary removeWaypointAtIndex:atIndex];
    if (self.delegate) {
        [self.delegate currentItineraryChanged];
    }
}

- (void)moveWaypointFromIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex
{
    [self.currentItinerary moveWaypointFromIndex:fromIndex toIndex:toIndex];
    if (self.delegate) {
        [self.delegate currentItineraryChanged];
    }
}

- (void)removeWaypointFromItinerary:(Waypoint *)waypoint
{
    if ([self.currentItinerary hasWaypoint:waypoint]) {
        [self.currentItinerary.waypoints removeObject:waypoint];
        if (self.delegate) {
            [self.delegate currentItineraryChanged];
        }
    }
}

- (void)setTransportModeCar
{
    [self changeTransportMode:kTransportModeCar];
}

- (void)setTransportModePedestrian
{
    [self changeTransportMode:kTransportModePedestrian];
}

- (void)setTransportModePublicTransport
{
    [self changeTransportMode:kTransportPublicTransport];
}

- (void)changeTransportMode:(TransportMode)newMode
{
    [self.currentItinerary changeTransportMode:newMode withChange:^(BOOL didChange) {
        if (didChange) {
            if (self.delegate) {
                [self.delegate currentItineraryChanged];
            }
        }
    }];
}

- (void)setCurrentItinerary:(Itinerary *)currentItinerary
{
    _currentItinerary = currentItinerary;
    
    if (self.delegate) {
        [self.delegate currentItineraryChanged];
    }
}

# pragma mark - bookmarks

+ (void)addCurrentItineraryToBookmarksWithName:(NSString *)bookmarkName
{
    [[self sharedManager] addCurrentItineraryToBookmarksWithName:bookmarkName];
}

- (void)addCurrentItineraryToBookmarksWithName:(NSString *)bookmarkName
{
    if (!bookmarkName || bookmarkName.length == 0) {
        bookmarkName = @"Itinerary";
    }
    
    NSData *itineraryData = [self dataFromItinerary:self.currentItinerary];
    
    NSDictionary *itineraryDictionary = @{ @"bookmarkName" : bookmarkName,
                                           @"itinerary"    : itineraryData,
                                           @"bookmarkDate" : [NSDate date] };
    
    NSArray *bookmarks = [self getBookmarksArray];
    
    NSMutableArray *bookmarksMutable = (bookmarks) ? [bookmarks mutableCopy] : [NSMutableArray new];
    
    [bookmarksMutable addObject:itineraryDictionary];
    
    [[NSUserDefaults standardUserDefaults] setObject:[bookmarksMutable copy] forKey:@"kItineraryBookmarks"];
    
}

+ (void)openBookmark:(NSDictionary *)bookmark
{
    [[self sharedManager] openBookmark:bookmark];
}

- (void)openBookmark:(NSDictionary *)bookmark
{
    if (bookmark) {
        NSData *itineraryData = bookmark[@"itinerary"];
        
        Itinerary *itinerary = [self itineraryFromData:itineraryData];
        
        self.currentItinerary = itinerary;
    }
}

+ (void)deleteBookmark:(NSDictionary *)bookmark completion:(void (^)(BOOL success))completion
{
    [[self sharedManager] deleteBookmark:bookmark completion:completion];
}

- (void)deleteBookmark:(NSDictionary *)bookmark completion:(void (^)(BOOL success))completion
{
    NSArray *bookmarks = [self getBookmarksArray];
    
    NSMutableArray *bookmarksMutable = (bookmarks) ? [bookmarks mutableCopy] : nil;
    
    if (bookmarksMutable) {
        [bookmarksMutable removeObject:bookmark];
        
        [[NSUserDefaults standardUserDefaults] setObject:[bookmarksMutable copy] forKey:@"kItineraryBookmarks"];
        
        if (completion) {
            completion(YES);
        }
        
    } else {
        if (completion) {
            completion(NO);
        }
    }
}

+ (void)openItineraryFromURL:(NSURL*)url
{
    [[self sharedManager] openItineraryFromURL:url];
}

- (void)openItineraryFromURL:(NSURL*)url
{
    Itinerary *itinerary = [self itineraryFromURL:url];
    if (itinerary)
        self.currentItinerary = itinerary;
}

# pragma mark - helpers

+ (BOOL)hasBookmarks
{
    return ([self getBookmarksArray].count > 0);
}

+ (NSArray*)getBookmarksArray
{
    return [[self sharedManager] getBookmarksArray];
}

- (NSArray*)getBookmarksArray
{
    NSArray *bookmarks = [[NSUserDefaults standardUserDefaults] objectForKey:@"kItineraryBookmarks"];
    
    return bookmarks;
}

+ (NSData *)dataFromItinerary:(Itinerary *)itinerary
{
    return [[self sharedManager] dataFromItinerary:itinerary];
}

- (NSData *)dataFromItinerary:(Itinerary *)itinerary
{
    NSData *itineraryData = [NSKeyedArchiver archivedDataWithRootObject:itinerary];
    
    return itineraryData;
}

+ (Itinerary *)itineraryFromData:(NSData *)itineraryData
{
    return [[self sharedManager] itineraryFromData:itineraryData];
}

- (Itinerary *)itineraryFromData:(NSData *)itineraryData
{
    Itinerary *itinerary = nil;
    @try {
         itinerary = [NSKeyedUnarchiver unarchiveObjectWithData:itineraryData];
    }
    @catch (NSException *exception) {
        NSLog(@"invalid data");
    }

    return itinerary;
}

+ (NSURL*)outgoingSharingUrlForItinerary:(Itinerary*)itinerary
{
    return [[self sharedManager] outgoingSharingUrlForItinerary:itinerary];
}

- (NSURL*)outgoingSharingUrlForItinerary:(Itinerary*)itinerary
{
    NSString *url = @"zdRouteApp://";
    
    url = [url stringByAppendingString:@"open/?"];
    
    NSString *itineraryEncoded = [self urlStringFromItinerary:itinerary];
    
    if (itineraryEncoded) {
        url = [url stringByAppendingString:@"itinerary="];
        url = [url stringByAppendingString:itineraryEncoded];
    }
    
    return [NSURL URLWithString:url];
}

- (NSString*)urlStringFromItinerary:(Itinerary*)itinerary
{
    NSData *nsdata = [self dataFromItinerary:itinerary];
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    NSString *urlEncoded = [base64Encoded stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    return urlEncoded;
}

- (Itinerary*)itineraryFromURL:(NSURL*)url
{
    NSDictionary *params = [self parametersFromURL:url];
    
    NSString *itineraryEncodedString = params[@"itinerary"];
    
    return [self itineraryFromUrlString:itineraryEncodedString];
}

- (NSDictionary *)parametersFromURL:(NSURL*)url
{
    NSString *urlString = [url query];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *param in [urlString componentsSeparatedByString:@"&"]) {
        NSArray *elements = [param componentsSeparatedByString:@"="];
        if([elements count] < 2) continue;
        [params setObject:[elements objectAtIndex:1] forKey:[elements objectAtIndex:0]];
    }
    
    return [params copy];
}

- (Itinerary*)itineraryFromUrlString:(NSString*)string
{
    Itinerary *decodedItinerary = nil;
    
    if (string) {
        NSString *urlDecoded = [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        int padLenght = (4 - (urlDecoded.length % 4)) % 4;
        NSString *paddedBase64String = [NSString stringWithFormat:@"%s%.*s", [urlDecoded UTF8String], padLenght, "=="];
        
        NSData *dataFromString = [[NSData alloc] initWithBase64EncodedString:paddedBase64String options:0];
        
        decodedItinerary = [self itineraryFromData:dataFromString];
    }
    
    return decodedItinerary;
}

@end
