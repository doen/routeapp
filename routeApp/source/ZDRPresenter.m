//
//  ZDRPresenter.m
//  routeApp
//
//  Created by Ziga Dolar on 25/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZDRPresenter.h"

@implementation ZDRPresenter

+ (UIViewController *)rootViewController
{
    return [[UIApplication sharedApplication] delegate].window.rootViewController;
}

+ (UIViewController*)topmostViewController
{
    UIViewController *topController = [self rootViewController];
    
    if ([topController isKindOfClass:[UITabBarController class]]) {
        topController = [(UITabBarController*)topController selectedViewController];
    }
    
    if ([topController isKindOfClass:[UINavigationController class]]) {
        topController = [(UINavigationController*)topController childViewControllers].lastObject;
    }
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

@end
