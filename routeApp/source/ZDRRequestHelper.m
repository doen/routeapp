//
//  ZDRRequestHelper.m
//  routeApp
//
//  Created by Ziga Dolar on 27/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRRequestHelper.h"

#import <CoreLocation/CoreLocation.h>

#import "ZDRItineraryManager.h"
#import "Itinerary.h"
#import "Waypoint.h"

#import "ZDRLocationManager.h"

NSString *const apiAppID   = @"DemoAppId01082013GAL";
NSString *const apiAppCode = @"AJKnXv84fjrb0KIHawS0Tg";

NSString *const apiSearchBaseURL = @"https://places.demo.api.here.com/places/v1/discover/search";
NSString *const apiRouteBaseURL  = @"https://route.cit.api.here.com/routing/7.2/calculateroute.json";

@implementation ZDRRequestHelper

+ (NSURL *)searchURLForSearchString:(NSString *)searchString
{
    NSString *searchURL = [apiSearchBaseURL stringByAppendingString:@"?"];
    
    searchURL = [searchURL stringByAppendingString:[NSString stringWithFormat:@"at=%@", [ZDRLocationManager sharedManager].currentCoordinatesString]];
    
    searchURL = [searchURL stringByAppendingString:@"&q="];
    searchURL = [searchURL stringByAppendingString:searchString];
    
    searchURL = [searchURL stringByAppendingString:@"&app_id="];
    searchURL = [searchURL stringByAppendingString:apiAppID];
    
    searchURL = [searchURL stringByAppendingString:@"&app_code="];
    searchURL = [searchURL stringByAppendingString:apiAppCode];
    
    searchURL = [searchURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    return [NSURL URLWithString:searchURL];
}

+ (NSURL *)routeURLWithItinerary:(Itinerary *)itinerary
{
    NSString *requestURL = [apiRouteBaseURL stringByAppendingString:@"?"];
    
    for (int i = 0; i < itinerary.waypoints.count; i++) {

        Waypoint *point = itinerary.waypoints[i];
        
        requestURL = [requestURL stringByAppendingString:[NSString stringWithFormat:@"waypoint%i=%f,%f", i, point.location.coordinate.latitude, point.location.coordinate.longitude]];
        
        if (i < itinerary.waypoints.count-1) {
            requestURL = [requestURL stringByAppendingString:@"&"];
        }
    }
    
    NSString *mode = @"car";
    
    switch ([ZDRItineraryManager sharedManager].currentItinerary.modeOfTransport) {
        case kTransportModeCar:
            mode = @"car";
            break;
        case kTransportModePedestrian:
            mode = @"pedestrian";
            break;
        case kTransportPublicTransport:
            mode = @"publicTransport";
            break;
            
        default:
            break;
    }
    
    requestURL = [requestURL stringByAppendingString:[NSString stringWithFormat:@"&mode=fastest;%@",mode]];
    
    requestURL = [requestURL stringByAppendingString:@"&app_id="];
    requestURL = [requestURL stringByAppendingString:apiAppID];
    
    requestURL = [requestURL stringByAppendingString:@"&app_code="];
    requestURL = [requestURL stringByAppendingString:apiAppCode];
    
    requestURL = [requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    return [NSURL URLWithString:requestURL];
}

@end
