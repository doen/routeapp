//
//  ZDRURLHandler.m
//  routeApp
//
//  Created by Ziga Dolar on 29/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "ZDRURLHandler.h"
#import <UIKit/UIKit.h>

#import "ZDRItineraryManager.h"

@implementation ZDRURLHandler

+ (BOOL)openURLscheme:(NSURL*)url sourceApplication:(NSString*)sourceApplication withAnnotation:(id)annotation application:(UIApplication*)application
{
    bool handledLink = NO;
    
    if (url) {
        if ([url.scheme isEqualToString:@"zdrouteapp"]) {
            if (url.query) {
                [ZDRItineraryManager openItineraryFromURL:url];
                handledLink = YES;
            }
        }
    }
    
    return handledLink;
}

@end
