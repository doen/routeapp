//
//  Waypoint.m
//  routeApp
//
//  Created by Ziga Dolar on 26/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import "Waypoint.h"

#import <CoreLocation/CoreLocation.h>
#import <AddressBookUI/AddressBookUI.h>

@interface Waypoint()

@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;

@end

@implementation Waypoint

# pragma mark - initialization

+ (instancetype)waypointWithDictionary:(NSDictionary *)dictionary
{
    return [[self alloc] initWithDictionary:dictionary];
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self.title   = dictionary[@"title"];
        self.address = [dictionary[@"vicinity"] stringByReplacingOccurrencesOfString:@"<br/>" withString:@", "];
        self.added = NO;
        self.droppedPin = NO;
        
        NSArray *position = dictionary[@"position"];
        
        if (position) {
            self.latitude  = position.firstObject;
            self.longitude = position.lastObject;
        }
    }
    return self;
}

+ (instancetype)waypointWithLocation:(CLLocation *)location title:(NSString *)title address:(NSString *)address
{
    return [[self alloc] initWithLocation:location title:title address:address];
}

- (instancetype)initWithLocation:(CLLocation *)location title:(NSString *)title address:(NSString *)address
{
    self = [super init];
    if (self) {
        self.title   = title;
        self.address = address;
        self.location = location;
        
        self.droppedPin = NO;
    }
    return self;
}

+ (instancetype)waypointFromDroppedPinAtLocation:(CLLocation *)location
{
    return [[self alloc] initFromDroppedPinAtLocation:location];
}

- (instancetype)initFromDroppedPinAtLocation:(CLLocation *)location
{
    self = [self initWithLocation:location title:@"Dropped Pin" address:nil];
    if (self) {
        self.droppedPin = YES;
    }
    return self;
}

- (void)setLocation:(CLLocation *)location
{
    self.latitude  = @(location.coordinate.latitude);
    self.longitude = @(location.coordinate.longitude);
}

# pragma mark - accessors

- (CLLocation *)location
{
    CLLocation *location = [[CLLocation alloc] initWithLatitude:self.latitude.doubleValue longitude:self.longitude.doubleValue];
    
    return location;
}

# pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.title      = [aDecoder decodeObjectForKey:@"title"    ];
        self.address    = [aDecoder decodeObjectForKey:@"address"  ];
        self.latitude   = [aDecoder decodeObjectForKey:@"latitude" ];
        self.longitude  = [aDecoder decodeObjectForKey:@"longitude"];
        self.droppedPin = [aDecoder decodeBoolForKey:@"droppedPin" ];
        
        return self;
    }
    return nil;
}

- (void)encodeWithCoder:(NSCoder *)anEncoder {
    [anEncoder encodeObject:self.title     forKey:@"title"    ];
    [anEncoder encodeObject:self.address   forKey:@"address"  ];
    [anEncoder encodeObject:self.latitude  forKey:@"latitude" ];
    [anEncoder encodeObject:self.longitude forKey:@"longitude"];
    [anEncoder encodeBool:self.droppedPin  forKey:@"droppedPin"];
}

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[self class]]) {
        Waypoint* waypoint = (Waypoint*)object;
        BOOL equal = YES;
        equal &= [waypoint.title isEqualToString:self.title];
        equal &= ([waypoint.address isEqualToString:self.address]);
        equal &= ([waypoint.location distanceFromLocation:self.location] == 0);
        return equal;
    } else {
        return NO;
    }
}

@end
