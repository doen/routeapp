//
//  ZDRLocationManager.h
//  routeApp
//
//  Created by Ziga Dolar on 28/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MKMapView;

@interface ZDRLocationManager : NSObject

+ (instancetype)sharedManager;

@property (nonatomic, weak) MKMapView *map;

- (NSString*)currentCoordinatesString;

@end
