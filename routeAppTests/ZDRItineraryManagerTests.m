//
//  ZDRItineraryManagerTests.m
//  routeApp
//
//  Created by Ziga Dolar on 29/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import <CoreLocation/CoreLocation.h>

#import "ZDRItineraryManager.h"
#import "Itinerary.h"
#import "Waypoint.h"

@interface ZDRItineraryManagerTests : XCTestCase

@end

@implementation ZDRItineraryManagerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    [ZDRItineraryManager sharedManager].currentItinerary = [Itinerary new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAddWaypointToItinerary
{
    Waypoint *waypoint = [Waypoint waypointFromDroppedPinAtLocation:[[CLLocation alloc] initWithLatitude:52.530929 longitude:13.384934]];
    
    XCTAssert(![[ZDRItineraryManager sharedManager].currentItinerary.waypoints containsObject:waypoint], @"Itinerary already contained object.");
    
    [[ZDRItineraryManager sharedManager] addWaypointToItinerary:waypoint];
    
    XCTAssert([[ZDRItineraryManager sharedManager].currentItinerary.waypoints containsObject:waypoint], @"Adding waypoint failed.");
}

- (void)testRemoveWaypointAtIndex
{
    Waypoint *waypoint1 = [Waypoint waypointFromDroppedPinAtLocation:[[CLLocation alloc] initWithLatitude:52.530929 longitude:13.384934]];
    
    [[ZDRItineraryManager sharedManager] addWaypointToItinerary:waypoint1];
    
    Waypoint *waypoint2 = [Waypoint waypointWithLocation:[[CLLocation alloc] initWithLatitude:52.530929 longitude:13.384934] title:@"Here" address:@"Invalidenstrasse 116, 10115 Berlin"];
    
    [[ZDRItineraryManager sharedManager] addWaypointToItinerary:waypoint2];
    
    XCTAssert([ZDRItineraryManager sharedManager].currentItinerary.waypoints.count == 2, @"Wrong number of waypoints in itinerary.");
    XCTAssertEqualObjects([ZDRItineraryManager sharedManager].currentItinerary.waypoints[1], waypoint2, @"Waypoint is not equal to waypoint in itinerary.");
    
    [[ZDRItineraryManager sharedManager] removeWaypointAtIndex:1];
    
    XCTAssert(![[ZDRItineraryManager sharedManager].currentItinerary.waypoints containsObject:waypoint2], @"Itinerary still contained object.");
    
}

- (void)testMoveWaypointFromIndexToIndex
{
    Waypoint *waypoint1 = [Waypoint waypointFromDroppedPinAtLocation:[[CLLocation alloc] initWithLatitude:52.530929 longitude:13.384934]];
    
    [[ZDRItineraryManager sharedManager] addWaypointToItinerary:waypoint1];
    
    Waypoint *waypoint2 = [Waypoint waypointWithLocation:[[CLLocation alloc] initWithLatitude:52.530929 longitude:13.384934] title:@"Here" address:@"Invalidenstrasse 116, 10115 Berlin"];
    
    [[ZDRItineraryManager sharedManager] addWaypointToItinerary:waypoint2];
    
    XCTAssertEqualObjects([ZDRItineraryManager sharedManager].currentItinerary.waypoints[1], waypoint2, @"Waypoint is not equal to waypoint in itinerary.");
    
    [[ZDRItineraryManager sharedManager] moveWaypointFromIndex:1 toIndex:0];
    
    XCTAssert([ZDRItineraryManager sharedManager].currentItinerary.waypoints.count == 2, @"Wrong number of waypoints in itinerary.");
    
    XCTAssertNotEqualObjects([ZDRItineraryManager sharedManager].currentItinerary.waypoints[1], waypoint2, @"Waypoint is equal to waypoint in itinerary.");
    XCTAssertEqualObjects([ZDRItineraryManager sharedManager].currentItinerary.waypoints[0], waypoint2, @"Waypoint is not equal to waypoint in itinerary.");
}

- (void)testRemoveWaypointFromItinerary
{
    Waypoint *waypoint = [Waypoint waypointFromDroppedPinAtLocation:[[CLLocation alloc] initWithLatitude:52.530929 longitude:13.384934]];
    
    XCTAssert(![[ZDRItineraryManager sharedManager].currentItinerary.waypoints containsObject:waypoint], @"Itinerary already contained object.");
    
    [[ZDRItineraryManager sharedManager] addWaypointToItinerary:waypoint];
    
    XCTAssert([[ZDRItineraryManager sharedManager].currentItinerary.waypoints containsObject:waypoint], @"Adding waypoint failed.");
    
    [[ZDRItineraryManager sharedManager] removeWaypointFromItinerary:waypoint];
    
    XCTAssert(![[ZDRItineraryManager sharedManager].currentItinerary.waypoints containsObject:waypoint], @"Removing waypoint failed.");
}

- (void)testSetTransportModeCar
{
    [[ZDRItineraryManager sharedManager] setTransportModePedestrian];
    
    XCTAssertNotEqual([ZDRItineraryManager sharedManager].currentItinerary.modeOfTransport, kTransportModeCar, @"Transport mode was already 'car'.");
    
    [[ZDRItineraryManager sharedManager] setTransportModeCar];
    
    XCTAssertEqual([ZDRItineraryManager sharedManager].currentItinerary.modeOfTransport, kTransportModeCar, @"Setting transport mode 'car' failed.");
}

- (void)testSetTransportModePedestrian
{
    XCTAssertNotEqual([ZDRItineraryManager sharedManager].currentItinerary.modeOfTransport, kTransportModePedestrian, @"Transport mode was already 'pedestrian'.");
    
    [[ZDRItineraryManager sharedManager] setTransportModePedestrian];
    
    XCTAssertEqual([ZDRItineraryManager sharedManager].currentItinerary.modeOfTransport, kTransportModePedestrian, @"Setting transport mode 'pedestrian' failed.");
}

- (void)testSetTransportModePublicTransport
{
    XCTAssertNotEqual([ZDRItineraryManager sharedManager].currentItinerary.modeOfTransport, kTransportPublicTransport, @"Transport mode was already 'public transport'.");
    
    [[ZDRItineraryManager sharedManager] setTransportModePublicTransport];
    
    XCTAssertEqual([ZDRItineraryManager sharedManager].currentItinerary.modeOfTransport, kTransportPublicTransport, @"Setting transport mode 'public transport' failed.");
}

@end
