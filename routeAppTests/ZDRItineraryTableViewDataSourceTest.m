//
//  ZDRItineraryTableViewDataSourceTest.m
//  routeApp
//
//  Created by Ziga Dolar on 29/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import <CoreLocation/CoreLocation.h>

#import "ZDRItineraryTableViewDataSource.h"
#import "ZDRWaypointTableViewCell.h"

#import "ZDRItineraryManager.h"
#import "Itinerary.h"
#import "Waypoint.h"

@interface ZDRItineraryTableViewDataSourceTest : XCTestCase

@property Waypoint *waypoint;
@property ZDRItineraryTableViewDataSource *itineraryTableViewDataSource;

@end

@implementation ZDRItineraryTableViewDataSourceTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    [ZDRItineraryManager sharedManager].currentItinerary = [Itinerary new];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:52.535339 longitude:13.42335];
    NSString *title = @"MarienBurger";
    NSString *address = @"Marienburger Str. 47, 10405 Berlin";
    
    self.waypoint = [Waypoint waypointWithLocation:location title:title address:address];
    
    [[ZDRItineraryManager sharedManager] addWaypointToItinerary:self.waypoint];
    
    self.itineraryTableViewDataSource = [ZDRItineraryTableViewDataSource new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    self.waypoint = nil;
    self.itineraryTableViewDataSource = nil;
}

- (void)testItemAtIndexPath
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    id itemAtIndexPath = [self.itineraryTableViewDataSource itemAtIndexPath:indexPath];
    
    XCTAssertEqualObjects(itemAtIndexPath, self.waypoint, @"Object at index path is not equal.");
}

- (void)testNumberOfRowsInSection
{
    NSInteger numberOfRows = [self.itineraryTableViewDataSource tableView:nil numberOfRowsInSection:0];
    
    XCTAssertEqual(numberOfRows, 1, @"Wrong number of rows in section.");
}

@end
