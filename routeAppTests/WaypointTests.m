//
//  WaypointTests.m
//  routeApp
//
//  Created by Ziga Dolar on 29/07/15.
//  Copyright (c) 2015 Ziga Dolar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>


#import <CoreLocation/CoreLocation.h>
#import "Waypoint.h"

@interface WaypointTests : XCTestCase

@end

@implementation WaypointTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testWaypointFromDroppedPinAtLocation
{
    CLLocation *location = [[CLLocation alloc] initWithLatitude:52.530929 longitude:13.384934];
    
    Waypoint *waypoint = [Waypoint waypointFromDroppedPinAtLocation:location];
    
    XCTAssert(waypoint, @"Waypoint creation failed.");
    XCTAssertEqualObjects(waypoint.title, @"Dropped Pin", @"Waypoint title incorrect.");
    XCTAssertEqual(waypoint.location.coordinate.latitude, location.coordinate.latitude, @"Waypoint latitude incorrect.");
    XCTAssertEqual(waypoint.location.coordinate.longitude, location.coordinate.longitude, @"Waypoint longitude incorrect.");
}

- (NSDictionary *)fakeWaypoints
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"fakeSearchData" ofType:@"json"];
    
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data
                                                             options:0
                                                               error:nil];
    
    return jsonDict;
}

- (void)testWaypointWithDictionary
{
    NSDictionary *dictionary = [[self fakeWaypoints][@"results"][@"items"] firstObject];
    
    Waypoint *waypoint = [Waypoint waypointWithDictionary:dictionary];
    
    XCTAssert(waypoint, @"Waypoint creation failed.");
    
    XCTAssertEqualObjects(waypoint.title, @"MarienBurger", @"Waypoint title incorrect.");
    XCTAssertEqual(waypoint.location.coordinate.latitude, 52.535339, @"Waypoint latitude incorrect.");
    XCTAssertEqual(waypoint.location.coordinate.longitude, 13.42335, @"Waypoint longitude incorrect.");
    XCTAssertEqualObjects(waypoint.address, @"Marienburger Str. 47, 10405 Berlin", @"Waypoint address incorrect.");
}

- (void)testWaypointWithLocationTitleAddress
{
    CLLocation *location = [[CLLocation alloc] initWithLatitude:52.535339 longitude:13.42335];
    NSString *title = @"MarienBurger";
    NSString *address = @"Marienburger Str. 47, 10405 Berlin";
    
    Waypoint *waypoint = [Waypoint waypointWithLocation:location title:title address:address];
    
    XCTAssert(waypoint, @"Waypoint creation failed.");
    
    XCTAssertEqualObjects(waypoint.title, title, @"Waypoint title incorrect.");
    XCTAssertEqual(waypoint.location.coordinate.latitude, location.coordinate.latitude, @"Waypoint latitude incorrect.");
    XCTAssertEqual(waypoint.location.coordinate.longitude, location.coordinate.longitude, @"Waypoint longitude incorrect.");
    XCTAssertEqualObjects(waypoint.address, address, @"Waypoint address incorrect.");

}

@end
